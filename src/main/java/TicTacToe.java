/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author ii drunkboy
 */
import java.util.Scanner;

public class TicTacToe {

    static char[][] table = {
        {'-', '-', '-'},
        {'-', '-', '-'},
        {'-', '-', '-'}
    };
    static char player = 'X';
    static int row, col;
    static Scanner kb = new Scanner(System.in);
    static boolean isFinish = false;
    static char winner = '-';
    static int count = 0;

    static void showWelcome() {
        System.out.println("Welcome to OX Game");
    }

    static void showTable() {
        System.out.println(" 123");
        for (int row = 0; row < table.length; row++) {
            System.out.print(row + 1);
            for (int col = 0; col < table[row].length; col++) {
                System.out.print(table[row][col]);
            }
            System.out.println("");
        }

    }

    static void showTurn() {
        System.out.println(player + " turn");

    }

    static void input() {
        while (true) {
            System.out.println("Please input Row Col: ");
            row = kb.nextInt() - 1;
            col = kb.nextInt() - 1;
            if (table[row][col] == '-') {
                table[row][col] = player;
                count++;
                break;
            }
            System.out.println("Error: Table at row and col is not empty!!!");
        }

    }

    static void checkCol() {
        for (int row = 0; row < 3; row++) {
            if (table[row][col] != player) {
                return;
            }
        }
        isFinish = true;
        winner = player;
    }

    static void checkRow() {
        for (int col = 0; col < 3; col++) {
            if (table[row][col] != player) {
                return;
            }
        }
        isFinish = true;
        winner = player;
    }

    static void checkX() {
                if ((table[0][0] == table[1][1]&& table[1][1] == table[2][2]
                     && table[2][2] == player) || (table[2][0] ==  table[1][1]
                     && table[1][1] == table[0][2] && table[0][2] == player)) {
                    isFinish = true;
                    winner = player;
        }
       return;
    }

    static void checkDraw() {
        if(winner == '-' && count == 9)
            isFinish = true;
    }
    static void checkWin() {
        checkRow();
        checkCol();
        checkX();
        checkDraw();

    }

    static void switchPlayer() {
        if (player == 'X') {
            player = 'O';
        } else {
            player = 'X';
        }
    }

    static void showResult() {
        if (winner == '-') {
            System.out.println("Draw!!");
        } else {
            System.out.println(winner + " Win!!!");
        }

    }

    static void showBye() {
        System.out.println("Bye bye ....");

    }

    public static void main(String[] args) {
        showWelcome();
        do {
            showTable();
            showTurn();
            input();
            checkWin();
            switchPlayer();
        } while (!isFinish);
        showResult();
        showBye();
    }
}
